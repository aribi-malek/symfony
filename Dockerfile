# Use an official PHP image as base
FROM php:8.1-cli

# Set the working directory inside the container
WORKDIR /var/www/html

# Install system dependencies
RUN apt-get update && apt-get install -y \
    unzip \
    git \
    libicu-dev \
    libxml2-dev \
    libzip-dev \
    && docker-php-ext-install -j$(nproc) intl pdo_mysql zip

# Install Symfony CLI
RUN curl -sS https://get.symfony.com/cli/installer | bash
RUN mv /root/.symfony*/bin/symfony /usr/local/bin/symfony

# Copy the rest of the application files
COPY . .

# Expose port 8000 to the outside world
EXPOSE 8000

# Command to run the Symfony application
CMD ["symfony", "serve", "--no-tls", "--port=8000", "--allow-http"]

